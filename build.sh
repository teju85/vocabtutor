#!/bin/bash
## USAGE:
##   build.sh [-h] [generate|commit|push|publish|copy]
##
## OPTIONS:
##   -h        Print this help and exit
##   generate  Generate the *.js dictionary files from *.json
##   commit    Commit all the changes locally. This will NOT push changes to the
##             remote
##   push      Push the changes to the remote repo
##   publish   Commit the changes and then push everything to the remote repo.
##             Same as "./build.sh generate commit push"
##   copy      Copy all the deployment files into the public folder for gitlab
##             hosting. This is to be mostly called from the gitlab CI.
##
## NOTE:
##  This should be called always from the root of the project!

set -e

SRC_BRANCH=master
DST=public

function generate() {
    for json in `ls *.json`; do
        js=`echo $json | sed -e 's/.json/.js/'`
        echo -n "var dictionary = " > $js
        cat $json >> $js
        echo ";" >> $js
    done
}

function commit() {
    read -p "Enter commit message: " cmtMsg
    git add -A
    git commit -m "$cmtMsg"
}

function push() {
    git push origin $SRC_BRANCH
}

function publish() {
    generate
    commit
    push
}

function printHelp() {
    grep '^##' $1 | sed -e 's/^##//' -e 's/^ //'
}

function checkRoot() {
    if [ ! -d ".git" ]; then
        echo "**ERROR** You are not at the root of this project!"
        exit -1
    fi
}

function copy() {
    rm -rf $DST
    mkdir -p $DST
    cp *.js $DST
    cp *.css $DST
    cp *.html $DST
}

checkRoot
while [ "$1" != "" ]; do
    case "$1" in
        "generate")
            generate
            shift;;
        "commit")
            commit
            shift;;
        "push")
            push
            shift;;
        "publish")
            publish
            shift;;
        "copy")
            copy
            shift;;
        "-h")
            printHelp $0
            shift
            exit 0;;
        *)
            echo "**ERROR** Bad arg '$1'! Use '-h' option for the right usage."
            shift
            exit -1;;
    esac
done
