// change this in order to vary the number of slots in the vocab table
const TotalRows = 8;
// DO NOT CHANGE THIS!
const TotalCols = 2;
// minimum number of empty slots in the vocab table after which 'fillWords' will
// be called
const MinEmpty = 5;
// delay (in ms) before clearing the vocab table selections
const ClearDelay = 300;
// timer delay (in ms) for every call to 'incrementTimer'
// (must be greater than 'ClearDelay')
const TimerInterval = 500;

function randInt(start, end) {
    let diff = end - start;
    let num = Math.floor(Math.random() * diff);
    return num + start;
}

class VocabTableState {
    constructor() {
        // create table and get its cells
        var t = document.getElementById("vocab");
        this.cells = Array(TotalRows);
        for (let i = 0; i < TotalRows; i++) {
            this.cells[i] = new Array(TotalCols);
            var row = document.createElement("tr");
            for (let j = 0; j < TotalCols; j++) {
                var col = document.createElement("td");
                var button = document.createElement("input");
                button.type = "button";
                button.className = "unselected";
                button.onclick = function () { globalState.next(i, j); };
                button.value = "";
                col.appendChild(button);
                row.appendChild(col);
                this.cells[i][j] = button;
            }
            t.appendChild(row);
        }
        this.reset();
    }

    // resets the full state
    reset() {
        this.pause();
        this.timeSoFar = 0;
        this.correctWords = 0;
        this.incorrectWords = 0;
        this.timerRunning = null;
        this.currentSelection = Array(TotalCols);
        for (let j = 0; j < TotalCols; j++) {
            this.currentSelection[j] = -1;
        }
        this.resetVocabTable();
        this.printStats();
        this.printTime();
    }

    resetVocabTable() {
        for (let i = 0; i < TotalRows; i++) {
            for (let j = 0; j < TotalCols; j++) {
                this.cells[i][j].value = "";
                this.cells[i][j].className = "unselected";
            }
        }
        this.currentWords = [];
        this.availableSlots = Array(TotalCols);
        for (let j = 0; j < TotalCols; j++) {
            this.availableSlots[j] = new Array(TotalRows);
            for (let i = 0; i < TotalRows; i++) {
                this.availableSlots[j][i] = i;
            }
        }
        this.resetCurrentSelection();
    }

    resetCurrentSelection() {
        for (let j = 0; j < TotalCols; ++j) {
            let row = this.currentSelection[j];
            if (row != -1) {
                this.cells[row][j].className = "unselected";
            }
            this.currentSelection[j] = -1;
        }
    }

    // prints the scores (called when updating the scores, eg from 'next')
    printStats() {
        document.getElementById("correct").innerHTML = this.correctWords;
        document.getElementById("incorrect").innerHTML = this.incorrectWords;
    }

    // prints the current time information (called from the timer logic)
    printTime() {
        document.getElementById("timer").innerHTML = Math.floor(this.timeSoFar/1000);
    }

    // (re)starts the timer
    start() {
        if (this.timerRunning) return;
        this.timerRunning = setInterval(incrementTimer, TimerInterval);
    }

    // stops the timer and thus also resets the vocab table
    pause() {
        if (!this.timerRunning) return;
        clearInterval(this.timerRunning);
        this.timerRunning = null;
        this.resetVocabTable();
    }

    // fills the empty slots with the next word(s)
    fillWords() {
        let rem = TotalRows - this.currentWords.length;
        if (rem < MinEmpty) return;
        // determine the new word and its location to place
        for (let i = 0; i < rem; i++) {
            let dicIdx = randInt(0, dictionary.length);
            let tmp = [];
            for (let j = 0; j < TotalCols; ++j) {
                let idx = randInt(0, this.availableSlots[j].length);
                let rowIdx = this.availableSlots[j][idx];
                tmp.push(rowIdx);
                this.availableSlots[j].splice(idx, 1);
                this.cells[rowIdx][j].value = dictionary[dicIdx][j];
            }
            this.currentWords.push(tmp);
        }
    }

    next(row, col) {
        for (let i = 0; i < TotalRows; i++) {
            this.cells[i][col].className = "unselected";
        }
        this.currentSelection[col] = row;
        this.cells[row][col].className = "selected";
        // still needs to match the columns
        if (!this.areAllColsSelected()) return;
        let matchId = this.check();
        if (matchId >= 0) {  // match found!
            this.correctWords++;
            for (let j = 0; j < TotalCols; ++j) {
                let row = this.currentSelection[j];
                this.cells[row][j].className = "correct";
            }
        } else {
            this.incorrectWords++;
            for (let j = 0; j < TotalCols; ++j) {
                let row = this.currentSelection[j];
                this.cells[row][j].className = "incorrect";
            }
        }
        setTimeout(markWordsAndDelete, ClearDelay, matchId);
        this.printStats();
    }

    areAllColsSelected() {
        for (let j = 0; j < TotalCols; j++) {
            if (this.currentSelection[j] == -1) return false;
        }
        return true;
    }

    check() {
        for (let i = 0; i < this.currentWords.length; i++) {
            let found = true;
            for (let j = 0; j < TotalCols; j++) {
                if (this.currentSelection[j] != this.currentWords[i][j]) {
                    found = false;
                    break;
                }
            }
            if (found) return i;
        }
        return -1;
    }

    unfillWord(matchId) {
        if (matchId >= 0) {
            for (let j = 0; j < TotalCols; j++) {
                let rowId = this.currentWords[matchId][j];
                this.availableSlots[j].push(rowId);
                this.cells[rowId][j].value = "";
            }
            this.currentWords.splice(matchId, 1);
        }
        this.resetCurrentSelection();
    }
}

let globalState = new VocabTableState();

function incrementTimer() {
    globalState.timeSoFar += TimerInterval;
    globalState.printTime();
    globalState.fillWords();
}

function markWordsAndDelete(matchId) {
    globalState.unfillWord(matchId);
}
